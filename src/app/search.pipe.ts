import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(s: any[], searchterm: any): any {
   if(!searchterm)
   {
     return s
   }
   {
     return s.filter(s=>s.emp_name.toLowerCase().indexOf(searchterm.toLowerCase())!==-1)
   }
  }

}
