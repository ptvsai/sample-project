import { Component, OnInit } from '@angular/core';
import { TransferdataService } from 'src/app/transferdata.service';

@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.css']
})
export class DeleteComponent implements OnInit {
receive:any[]=[]
  constructor(private service:TransferdataService) { }

  ngOnInit() {
    this.receive=this.service.recievedeldata()
  }

}
