import { Component, OnInit } from '@angular/core';
import { TransferdataService } from 'src/app/transferdata.service';


@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {
emp:any[]=[];
  constructor(private service:TransferdataService) { }

  ngOnInit() {
  }
  sendto(x)
  {
    this.emp.push(x)
    this.service.sendtoservice1(this.emp)
  }

}
