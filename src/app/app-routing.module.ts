import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddComponent } from './main/add/add.component';
import { DeleteComponent } from './main/delete/delete.component';
import { EditComponent } from './main/edit/edit.component';
import { ReadComponent } from './main/read/read.component';
import { MainComponent } from './main/main.component';


const routes: Routes = [
  {
    path:"",
    redirectTo:"/main",
    pathMatch:"full",

  }
  ,{
  path:"main",
  component:MainComponent,
children:[
 {
    path:"Add",
    component:AddComponent
  },
  {
    path:"Delete",
    component:DeleteComponent
  },
  {
    path:"Edit",
    component:EditComponent
  },
  {
    path:"Read",
    component:ReadComponent
  },
]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
