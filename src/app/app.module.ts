import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainComponent } from './main/main.component';
import { AddComponent } from './main/add/add.component';
import { DeleteComponent } from './main/delete/delete.component';
import { ReadComponent } from './main/read/read.component';
import { EditComponent } from './main/edit/edit.component';
import {FormsModule}      from "@angular/forms"
import {HttpClientModule} from "@angular/common/http";
import { SearchPipe } from './search.pipe'

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    AddComponent,
    DeleteComponent,
    ReadComponent,
    EditComponent,
    SearchPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
