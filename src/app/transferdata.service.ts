import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {  HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TransferdataService {
read:any[]=[]
delete:any[]=[]
g:any[]=[]
  constructor(private hc:HttpClient) { }
  sendtoservice1(r)
  {
    this.read=r
  }
  recievereaddata()
  {
    return this.read
  }
  sendtoservice2(d)
  {
    this.delete=d
  }
  recievedeldata()
  {
    return this.delete
  }
  getdata():Observable<any[]>
  {
    return this.hc.get<any[]>('/assets/emp.json')
  }
}
